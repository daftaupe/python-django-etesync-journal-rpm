# Created by pyp2rpm-3.3.2
%global pypi_name django-etesync-journal

Name:           python-%{pypi_name}
Version:        1.2.2
Release:        1%{?dist}
Summary:        The server side implementation of the EteSync protocol

License:        AGPLv3
URL:            https://www.etesync.com/
Source0:        https://files.pythonhosted.org/packages/source/d/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

 
BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)

%description
This is a reusable django app that implements the server side of EteSync

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3-django python3-djangorestframework python3-drf-nested-routers

%description -n python3-%{pypi_name}
This is a reusable django app that implements the server side of EteSync

%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install

%files -n python3-%{pypi_name}
%license LICENSE
%doc README.md
%{python3_sitelib}/journal
%{python3_sitelib}/django_etesync_journal-%{version}-py?.?.egg-info

%changelog
* Sat Sep 19 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> - 1.2.2-1
- Update do 1.2.2

* Wed Jan 29 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> - 1.2.0-2
- Fix dependencies for python3 version of the package

* Fri Jan 24 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> - 1.2.0-1
- Update to 1.2.0

* Tue Jan 21 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> - 1.1.0-1
- Update to 1.1.0

* Mon Nov 12 2019 rpm - 1.0.3-1
- Update to 1.0.3

* Mon May 20 2019 rpm - 1.0.2-2
- Add missing dependencies

* Thu May 02 2019 rpm - 1.0.2-1
- Initial package.
